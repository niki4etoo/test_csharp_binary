const http = require('node:http');
const url = require('node:url');
const { exec } = require('child_process');

const hostname = '127.0.0.1';
const port = 5000;

const server = http.createServer((req, res) => {

    let pathname = url.parse(req.url, true).pathname;

    if (pathname === '/sharp') {
        console.log('You are in the path for csharp files');

        let output = "";

        exec('mono ./test/test.exe', function (err, stdout) { // Тука Стъки променяш 'mono ./test/test.exe' към 'твоя компилатор ./test/test.exe'
            if (err) {
                return;
            } else {
                res.status = 200;
                res.writeHead(res.status, { 'Content-Type': 'text-plain' });
                res.write(stdout);
                res.end();
            }

        });
    }

});

server.listen(port, hostname, () => {
    console.log(`Server is listening on ${hostname}:${port}`);
})

//За стартирането
// https://nodejs.org/en/download/
// след инсталация провери в cmd -> node --version
// стартираш сървъра в cmd -> node index.js (текущата папка)
// http://localhost:5000/sharp 
// в конзолата трябва да излезе резултата "You are in the path for csharp files"
// в браузърчето трябва да излезе резултата от csharp exe-то
// Ако не работи, ми плесни един шамар зад врата.
